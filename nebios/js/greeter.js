class GreeterScreen {
	constructor() {
		this._screenGreeter = document.querySelector('#screen-greeter');
		this._passwordInput = document.querySelector('#input-password');
		this._buttonScreenGreeter = document.querySelector('#button-greeter-screen');
		this._arrowIndicatorGreeter = document.querySelector('#arrow-indicator-greeter');
		this._loginDiv = document.querySelector('#login-form');
		this._loginBtn = document.querySelector('#buttons-main-screen');
		this._bodyBackground = document.querySelector('.body-background');
		this._screenGreeterVisible = true;
		this._buttonGreeterClickEvent();
		this._arrowIndicatorClickEvent();
	}

	getGreeterVisibility() {
		return this._screenGreeterVisible;
	}

	_buttonGreeterClickEvent() {
		this._buttonScreenGreeter.addEventListener(
			'click',
			() => {
				this._showGreeter();
			}
		);
	}

	_arrowIndicatorClickEvent() {
		this._arrowIndicatorGreeter.addEventListener(
			'click',
			() => {
				this._hideGreeter();
			}
		);
	}

	_showGreeter() {
		userProfile.rotateProfilePicture();
		this._screenGreeter.classList.remove('screen-greeter-hide');
		this._loginDiv.classList.remove('logindiv-show');
		this._loginBtn.classList.remove('loginbtn-show');
		this._bodyBackground.classList.remove('bodybg-show');
		this._passwordInput.blur();
		this._screenGreeterVisible = true;
	}

	_hideGreeter() {
		userProfile.rotateProfilePicture();
		this._screenGreeter.classList.add('screen-greeter-hide');
		this._loginDiv.classList.add('logindiv-show');
		this._loginBtn.classList.add('loginbtn-show');
		this._bodyBackground.classList.add('bodybg-show');
		this._passwordInput.focus();
		this._screenGreeterVisible = false;
	}

	toggleGreeter() {
		if (!this._screenGreeterVisible) {
			this._showGreeter();
		} else {
			this._hideGreeter();
		}
	}
}
